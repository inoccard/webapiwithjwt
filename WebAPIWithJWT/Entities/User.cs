﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPIWithJWT.Entities
{
    public class User
    {
        public Guid Id { get; set; }
        public Guid Name { get; set; }
        public Guid Password { get; set; }
    }
}
