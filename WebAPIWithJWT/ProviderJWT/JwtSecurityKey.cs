﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebAPIWithJWT.ProviderJWT
{
    /// <summary>
    /// Classe para gerar chave de autenticação
    /// </summary>
    public class JwtSecurityKey
    {
        /// <summary>
        /// Cria e retorna chave
        /// </summary>
        /// <param name="secret"></param>
        /// <returns></returns>
        public static SymmetricSecurityKey Create(string secret)
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(secret));
        }
    }
}