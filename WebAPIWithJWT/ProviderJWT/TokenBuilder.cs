﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPIWithJWT.ProviderJWT
{
    public class TokenBuilder
    {
        private SecurityKey securityKey = null;
        private string subject = "";
        private string issuer = "";
        private string audience = "";
        private Dictionary<string, string> claims = new Dictionary<string, string>();
        private int expiryMinutes = 5;

        public TokenBuilder AddSecurityKey(SecurityKey securityKey)
        {
            this.securityKey = securityKey;
            return this;
        }
        public TokenBuilder AddSubject(string subject)
        {
            this.subject = subject;
            return this;
        }
        public TokenBuilder AddIssuer(string issuer)
        {
            this.issuer = issuer;
            return this;
        }
        public TokenBuilder AddSAudience(string audience)
        {
            this.audience = audience;
            return this;
        }
        public TokenBuilder AddClaim(string type, string value)
        {
            this.claims.Add(type, value);
            return this;
        }
        public TokenBuilder AddClaims(Dictionary<string, string> claims)
        {
            this.claims.Union(claims);
            return this;
        }
        
        public TokenBuilder AddExpiry(int expiryMinutes)
        {
            this.expiryMinutes = expiryMinutes;
            return this;
        }
    }
}
